pub type Rect = ([f32; 2], [f32; 2]);

pub trait Theme {
    fn border_size(&mut self, style: (u8, u8)) -> f32;
    fn rect_min_size(&mut self, style: (u8, u8)) -> [f32; 2] {
        [self.border_size(style) * 2.0; 2]
    }
    fn text_min_size(&mut self, font: (u8, u8), text: &str) -> [f32; 2];
}

pub trait Renderer {
    type T: Theme;

    fn clear(&mut self, t: &mut Self::T, style: (u8, u8));
    fn rect(&mut self, t: &mut Self::T, rect: Rect, style: (u8, u8));
    fn text(&mut self, t: &mut Self::T, rect: Rect, style: (u8, u8), font: (u8, u8), text: &str);
}
